import os
import telebot
import config
from flask import Flask, request

bot = telebot.TeleBot(config.token)
server = Flask(__name__)


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
        bot.reply_to(message, "Howdy, how are you doing?")


@bot.message_handler(func=lambda m: True)
def echo_all(message):
        bot.reply_to(message, message.text)


@server.route('/' + config.token, methods=['POST'])
def getMessage():
    bot.process_new_updates([telebot.types.Update.de_json(request.stream.read().decode("utf-8"))])
    return "!", 200


@server.route("/")
def webhook():
    bot.remove_webhook()
    bot.set_webhook(url=config.app_fqdn + config.token)
    return "!", 200


@server.route("/unset")
def unset_webhook():
    bot.remove_webhook()
    return "unset", 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)))
